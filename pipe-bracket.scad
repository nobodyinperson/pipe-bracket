/* [Bracket] */
bracket_thickness = 5;
bracket_height = 20;
bracket_width = 60;

/* [Pipe] */
// only for demonstration
pipe_diameter = 19.2;
pipe_hole_diameter = 22.3;
pipe_hole_offset = 6.5;
// clang-format off
// what distance relative to the pipe_hole_diameter to snap the pipe in. Set to 1 to disable the gap so the pipe will need to be pushed through.
pipe_hole_snapin_relative_distance = 0.5; // [0:0.01:1]
// clang-format on

/* [Screw Holes] */
screw_hole_diameter = 4;
screw_sink_diameter = 8;
screw_sink_depth = 3;

/* [Rendering] */
$fa = 0.5;
$fs = 0.5;

// helpers

epsilon = 1;
plane_thickness = bracket_thickness * 4;
plane_width = max(bracket_width, bracket_height) * 2;
plane_height = plane_width;
pipe_length = plane_width;
bracket_depth = bracket_thickness + pipe_hole_diameter + pipe_hole_offset;

module
move_to_pipe()
{
  for (i = [0:($children - 1)])
    rotate([ 90, 0, 0 ]) children(i);
}

module
move_to_plane()
{
  for (i = [0:($children - 1)])
    translate([ 0, 0, -(pipe_hole_diameter / 2 + pipe_hole_offset) ])
      children(i);
}

// the pipe for demonstration
% move_to_pipe() cylinder(d = pipe_diameter, h = pipe_length, center = true);

// the plane for demonstration
% move_to_plane() translate([ 0, 0, -plane_thickness / 2 ])
    cube([ plane_width, plane_height, plane_thickness ], center = true);

difference()
{
  intersection()
  {
    union()
    {
      // the bracket base plate
      move_to_plane() translate([ 0, 0, +bracket_thickness / 2 ]) cube(
        [ bracket_width, bracket_height, bracket_thickness ], center = true);

      hull()
      {
        // the pipe holder
        move_to_pipe() cylinder(r = pipe_hole_diameter / 2 + bracket_thickness,
                                h = bracket_height,
                                center = true);
        move_to_plane() linear_extrude(epsilon) projection() move_to_pipe()
          cylinder(r = pipe_hole_diameter / 2 + bracket_thickness,
                   h = bracket_height,
                   center = true);
      }
    }
    // only keep material outside of the plane
    move_to_plane() translate([ 0, 0, +bracket_depth / 2 ])
      cube([ bracket_width, bracket_height, bracket_depth ], center = true);
  }

  // hole for the pipe
  move_to_pipe() cylinder(
    d = pipe_hole_diameter, h = bracket_height + epsilon, center = true);

  // gap for the pipe
  if (pipe_hole_snapin_relative_distance != 1) {
    hull()
    {
      translate(
        [ 0, 0, -pipe_hole_snapin_relative_distance * pipe_hole_diameter ])
        move_to_pipe() cylinder(
          d = pipe_hole_diameter, h = bracket_height + epsilon, center = true);
      move_to_plane() move_to_pipe() cylinder(
        d = pipe_hole_diameter, h = bracket_height + epsilon, center = true);
    }
  }

  // screw sinks
  for (i = [ -1, 1 ]) {
    // move one screw hole to either side
    translate([
      i * ((pipe_hole_diameter / 2 + bracket_thickness) +
           (bracket_width - (pipe_hole_diameter + 2 * bracket_thickness)) / 4),
      0,
      0
    ]) move_to_plane()
    {
      translate([ 0, 0, bracket_thickness - screw_sink_depth ])
        cylinder(d1 = screw_hole_diameter,
                 d2 = screw_sink_diameter,
                 h = screw_sink_depth + epsilon);
      translate([ 0, 0, bracket_thickness / 2 - epsilon ])
        cylinder(d = screw_hole_diameter,
                 h = bracket_thickness + epsilon,
                 center = true);
    }
  }
}
